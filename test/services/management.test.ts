import app from '../../src/app';

describe('\'management\' service', () => {
  it('registered the service', () => {
    const service = app.service('management');
    expect(service).toBeTruthy();
  });
});
