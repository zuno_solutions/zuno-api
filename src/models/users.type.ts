export interface UserType {
    name: string;
    id: number;
    lastname: string;
    username: string;
    email: string;
    phone: string;
    password: string;
    auth0Id: string;
    googleId: string;
    facebookId: string;
    isVerified: boolean;
    verifyShortToken: string;
    verifyExpires: Date;
    verifyChanges: JSON;
    resetShortToken: string;
    resetExpires: Date;
  }