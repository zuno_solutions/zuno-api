// Initializes the `management` service on path `/management`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations/declarations";
import { Management } from "./management.class";
import hooks from "./management.hooks";
import FeathersAuthenticationManagement from "feathers-authentication-management";
import notifier from "./notifier";
//import management from "feathers-authentication-management";

//const notifier = management.notifier;

// Add this service to the service type index
declare module "../../declarations/declarations" {
  interface ServiceTypes {
    management: Management & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const identifyUserProps = app.get("identifyUserProps");
  const shortTokenLen = app.get("shortTokenLen");
  const shortTokenDigits = app.get("shortTokenDigits");
  const options = {
    paginate: app.get("paginate"),
    identifyUserProps,
    shortTokenLen,
    shortTokenDigits,
  };

  // Initialize our service with any options it requires
  app.configure(FeathersAuthenticationManagement(options, notifier(app)));
  app.use("/management", new Management(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("management");

  service.hooks(hooks);
}
