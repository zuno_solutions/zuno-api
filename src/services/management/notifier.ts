import { Application } from "../../declarations/declarations";
import { UserType } from "../../models/users.type";

interface SmsData {
  from: string | undefined;
  to: string;
  body: string;
}

export default function (app: any): any {
  function sendSms(sms: SmsData) {
    return app
      .service("sms")
      .create(sms)
      .then(() => {
        console.log(`SMS sent to ${sms.to}`);
      })
      .catch((err: string | undefined) => {
        console.log({ err });
        throw Error(err);
      });
  }

  function setSms(type: string, user: UserType): SmsData {
    let code: string;
    switch (type) {
      case "resendVerifySignup": // send another sms with link for verifying user's email address
        code = user.verifyShortToken;
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: `Your verification code is: ${code}`,
        };
        break;
      case "verifySignup": // inform that user's email is now confirmed
        code = user.verifyShortToken;
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: `[Zuno] - Your verification code is: ${code}`,
        };
        break;
      case "sendResetPwd": // inform that user's email is now confirmed
        code = user.verifyShortToken;
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: `Your reset code is: ${code}`,
        };
        break;
      case "resetPwd": // inform that user's email is now confirmed
        code = user.verifyShortToken;
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: `Your reset code is: ${code}`,
        };
        break;
      case "passwordChange": // Inform the user of a password change
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: "Your password was changed",
        };
        break;
      case "identityChange": // Inform the user of an identity change
        code = user.verifyShortToken;
        return {
          from: process.env.TWILIO_NUMBER,
          to: user.phone,
          body: "Your password was changed",
        };
        break;
      default:
        return {
          from: process.env.TWILIO_NUMBER,
          to: "",
          body: "Your password was changed",
        };
        break;
    }
  }

  return {
    notifier: (type: string, user: UserType) => {
      const sms: SmsData = setSms(type, user);
      return sendSms(sms);
    },
  };
}
