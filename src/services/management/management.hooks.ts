import sendVerificationSms from "../../hooks/send-verification-sms";
import * as feathersAuthentication from "@feathersjs/authentication";
import { HookContext } from "@feathersjs/feathers";
const { authenticate } = feathersAuthentication.hooks;
import commonHooks from "feathers-hooks-common";
import sendVerificationEmail from "../../hooks/send-verification-email";

const isAction = (actions: Array<string>) => {
  return (hook: HookContext) => actions.includes(hook.data.action);
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [
      //sendVerificationSms()
    ],
    find: [],
    get: [],
    create: [
      commonHooks.iff(
        isAction(["passwordChange", "identityChange"]),
        authenticate("jwt"),
        () => {console.log("MANAGEMENT-HOOKS.AFTER.CREATE IF ACTION IN (passwordChange, identityChange)")}
      ),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
