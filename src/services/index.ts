import { Application } from "../declarations/declarations";
import users from "./users/users.service";
import sms from "./sms/sms.service";
import management from "./management/management.service";
import mails from "./mails/mails.service";
import userHospitalInfo from './user_hospital_info/user_hospital_info.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(users);
  app.configure(sms);
  app.configure(management);
  app.configure(mails);
  app.configure(userHospitalInfo);
}
