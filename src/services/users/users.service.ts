// Initializes the `users` service on path `/users`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations/declarations";
import { Users } from "./users.class";
import createModel from "../../models/users.model";
import hooks from "./users.hooks";

//import { authenticate } from "@feathersjs/authentication";
//import commonHooks from "feathers-hooks-common";
//import { verifyHooks } from "feathers-authentication-management";
//import sendVerificationSms from "../../hooks/sendVerificationSms";
//import { hashPassword, protect } from "@feathersjs/authentication-local";

// Add this service to the service type index
declare module "../../declarations/declarations" {
  interface ServiceTypes {
    users: Users & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/users", new Users(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("users");

  service.hooks(hooks);
}
