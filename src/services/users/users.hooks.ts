import * as feathersAuthentication from "@feathersjs/authentication";
import * as local from "@feathersjs/authentication-local";
import FeathersAuthenticationManagement from "feathers-authentication-management";
import stringifyVerifyChanges from "../../hooks/stringify-verify-changes";
// Don't remove this comment. It's needed to format import lines nicely.

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;
const verifyHooks = FeathersAuthenticationManagement.hooks;

import commonHooks from "feathers-hooks-common";
import sendVerificationSms from "../../hooks/send-verification-sms";
import sendVerificationEmail from "../../hooks/send-verification-email";


export default {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [authenticate("jwt")],
    create: [
      verifyHooks.addVerification(),
      stringifyVerifyChanges(),
      hashPassword("password"),
    ],
    update: [
      hashPassword("password"),
      authenticate("jwt"),
      stringifyVerifyChanges(),
    ],
    patch: [
      //hashPassword("password"), // Commented because the password is being hashed twice
      () => {console.log("USERS-HOOKS.BEFORE.PATCH")},
      //authenticate("jwt"),
      commonHooks.iff(
        commonHooks.isProvider("external"),
        commonHooks.preventChanges(
          false,
          "phone",
          "username",
          "isVerified",
          "verifyToken",
          "verifyShortToken",
          "verifyExpires",
          "verifyChanges",
          "resetToken",
          "resetShortToken",
          "resetExpires"
        )
      ),
      stringifyVerifyChanges(),
    ],
    remove: [authenticate("jwt")],
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect(
        "password",
        "_computed",
        "verifyExpires",
        "resetExpires",
        "verifyChanges"
      ),
    ],
    find: [],
    get: [],
    create: [sendVerificationEmail(), sendVerificationSms(), verifyHooks.removeVerification()],
    update: [],
    patch: [sendVerificationEmail()],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
