// Initializes the `sms` service on path `/sms`
import { ServiceAddons } from "@feathersjs/feathers";
import { Application } from "../../declarations/declarations";
import { Sms } from "./sms.class";
import hooks from "./sms.hooks";
import * as feathersTwilio from "feathers-twilio";

//import smsService from "feathers-twilio/lib";
//import accountSid from process.env.TWILIO_SID;
//import authToken from process.env.TWILIO_AUTH_TOKEN;

// Add this service to the service type index
declare module "../../declarations/declarations" {
  interface ServiceTypes {
    sms: Sms & ServiceAddons<any>;
  }
}
const smsService = feathersTwilio.sms;
const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

export default function (app: Application): void {
  const options = {
    paginate: app.get("paginate"),
    accountSid,
    authToken,
  };

  // Initialize our service with any options it requires
  //app.use('/sms', new Sms(options, app));
  app.use("/sms", smsService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("sms");

  service.hooks(hooks);
}
