import { Application } from "@feathersjs/express";
import { ServiceMethods } from "@feathersjs/feathers";
import { createTransport, Transporter, SendMailOptions } from "nodemailer";
import sgMail from "@sendgrid/mail";

sgMail.setApiKey(process.env.SENDGRID_API_KEY || "");

export class Mails implements Partial<ServiceMethods<SendMailOptions>> {
  private transporter: Transporter;

  constructor(app: Application) {
    // We initialize the transporter.
    this.transporter = createTransport(app.get("mailer"));
  }

  /**
   * We send the email.
   * @param data
   * @returns
   */

  async create(data: any): Promise<any> {
    // return await this.transporter.sendMail(data);
    (async () => {
      try {
        return await sgMail.send(data);
      } catch (error: any) {
        console.error(error);

        if (error.response) {
          console.error(error.response.body);
        }
        return error;
      }
    })();
  }
}
