import { Service, KnexServiceOptions } from "feathers-knex";
import { Application } from "../../declarations/declarations";

export class UserHospitalInfo extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    super({
      ...options,
      name: "user_hospital_info"
    });
  }
}

// import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
// import { Application } from '../../declarations/declarations';

// interface Data {}

// interface ServiceOptions {}

// export class UserHospitalInfo implements ServiceMethods<Data> {
//   app: Application;
//   options: ServiceOptions;

//   constructor (options: ServiceOptions = {}, app: Application) {
//     this.options = options;
//     this.app = app;
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async find (params?: Params): Promise<Data[] | Paginated<Data>> {
//     return [];
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async get (id: Id, params?: Params): Promise<Data> {
//     return {
//       id, text: `A new message with ID: ${id}!`
//     };
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async create (data: Data, params?: Params): Promise<Data> {
//     if (Array.isArray(data)) {
//       return Promise.all(data.map(current => this.create(current, params)));
//     }

//     return data;
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
//     return data;
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
//     return data;
//   }

//   // eslint-disable-next-line @typescript-eslint/no-unused-vars
//   async remove (id: NullableId, params?: Params): Promise<Data> {
//     return { id };
//   }
// }
