// Initializes the `user_hospital_info` service on path `/user-hospital-info`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations/declarations';
import { UserHospitalInfo } from './user_hospital_info.class';
import createModel from "../../models/user_hospital_info.model";
import hooks from './user_hospital_info.hooks';

// Add this service to the service type index
declare module '../../declarations/declarations' {
  interface ServiceTypes {
    'user_hospital_info': UserHospitalInfo & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/user_hospital_info', new UserHospitalInfo(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('user_hospital_info');

  service.hooks(hooks);
}
