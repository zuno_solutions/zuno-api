/* eslint-disable linebreak-style */
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
var moment = require("moment");

type EmailReceiverSender = {
  email: string;
  name: string;
};

type Personalization = {
  to: Array<EmailReceiverSender>;
  dynamic_template_data: {
    verification_link?: string;
    verification_code?: string;
    user_name?: string;
    user_lastname?: string;
  };
};

type EmailData = {
  personalizations?: Array<Personalization>;
  from: EmailReceiverSender;
  to?: EmailReceiverSender;
  subject?: string;
  template_id?: string;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    // console.log("sendVerificationEmail");
    // console.log(context.method);
    // console.log(context.path);
    // console.log(context.data);
    // console.log(context.result);

    const data: EmailData = {
      from: {
        email: process.env.FROM_EMAIL_SENDER || "",
        name: process.env.FROM_EMAIL_NAME || "",
      },
    };

    switch (context.path) {
      case "users":
        switch (context.method) {
          case "create":
            if (context.result.id) {
              const user = await context.app
                .service("users")
                .get(context.result.id);

              data.personalizations = [
                {
                  to: [
                    {
                      email: context.result.email,
                      name: "",
                    },
                  ],
                  dynamic_template_data: {
                    verification_link: `${process.env.FRONT_END_HEROKU_SERVER}confirm-email?userid=${context.result.id}&token=${user.verifyToken}`,
                  },
                },
              ];
              data.subject = "Zuno - Email verification";
              data.template_id = process.env.EMAIL_TEMPLATE_ID_VERIFICATION;

              console.log("options_create");
              console.log(data);
              return await context.app.service("mails").create(data);
            } else {
              return context;
            }
            break;
        }
        break;

      case "authentication":
        // SENDING VERIFICATION TOKEN AFTER LOGIN
        var shortToken = Math.floor(100000 + Math.random() * 900000);
        // console.log("shortToken ",shortToken);

        var timestamp = moment().add(15, "m").valueOf();
        // console.log("timestamp ",timestamp);

        if (context.method === "create") {
          context.app.service("users").patch(context.result.user.id, {
            verifyshorttoken: shortToken,
            isverified: 0,
            verifyexpires: timestamp,
          });

          // console.log("options_authentication");
          data.personalizations = [
            {
              to: [
                {
                  email: context.result.user.email,
                  name: "",
                },
              ],
              dynamic_template_data: {
                verification_code: `${shortToken}`,
                user_name: context.result.user.email,
                user_lastname: "",
              },
            },
          ];
          data.subject = "Zuno - PIN Code";
          data.template_id = process.env.EMAIL_TEMPLATE_PIN_CODE;

          // console.log(data);
          return await context.app.service("mails").create(data);
        }
        break;

      default:
        console.log("default");
        // console.log(options);
        // context.app.service("mails").create(options);
        return context;
        break;
    }

    return context;
  };
};
