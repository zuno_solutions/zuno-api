// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
import sendVerificationEmail from "./send-verification-email";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    console.log(context.method);
    console.log(context.path);
    // console.log(context.result);
    if (
      context.path === "authentication" &&
      context.method === "create" &&
      context.result.authentication.strategy === "local"
    ) {
      console.log("enviar verification email");
      sendVerificationEmail();
      console.log("SALIO verification email");
    }
    return context;
  };
};
