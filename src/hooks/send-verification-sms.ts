// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from "@feathersjs/feathers";
//import { Management } from "../services/management/management.class";

import accountService from "../services/management/notifier";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    if (!context.params.provider) {
      return context;
    }
    const user = context.result;
    if (context.data && context.data.phone && user) {
      accountService(context.app).notifier("resendVerifySignup", user);
      return context;
    }
    return context;
  };
};
