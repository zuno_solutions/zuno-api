import sendAuthenticationVerificationCode from './hooks/send-authentication-verification-code';// Application hooks that run for every service
import sendVerificationEmail from './hooks/send-verification-email';
// Don't remove this comment. It's needed to format import lines nicely.

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      sendVerificationEmail()
       //sendAuthenticationVerificationCode()
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
