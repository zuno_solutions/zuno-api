// Update with your config settings.

module.exports = {
  development: {
    client: "mysql2",
    connection:
      "mysql://root:SdCho2fs7xX7JerkN9LP@zuno-db-01.csm3un4ykbsi.us-east-1.rds.amazonaws.com:3306/zuno",
    // connection: {
    //   database: "zuno",
    //   user: "root",
    //   password: "SdCho2fs7xX7JerkN9LP",
    // },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
      extension: "ts",
      directory: "./db/migrations",
    },
  },

  developmentheroku: {
    client: "mysql2",
    connection:
      "mysql://root:SdCho2fs7xX7JerkN9LP@zuno-db-01.csm3un4ykbsi.us-east-1.rds.amazonaws.com:3306/zuno",
    // connection: {
    //   database: "zuno",
    //   user: "root",
    //   password: "SdCho2fs7xX7JerkN9LP",
    // },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
      extension: "ts",
      directory: "./db/migrations",
    },
  },

  staging: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },

  production: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
