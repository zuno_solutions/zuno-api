import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // creating FK user_hospital_info_fk
  return knex.schema.alterTable("user_hospital_info", function (table) {
    table
      .foreign("user_id")
      .references("users.id")
      .withKeyName("user_hospital_info_fk");
  });
}

export async function down(knex: Knex): Promise<void> {
  // dropping FK user_hospital_info_fk
  return knex.schema.alterTable("user_hospital_info", function (table) {
    table.dropForeign("user_id", "user_hospital_info_fk");
  });
}
