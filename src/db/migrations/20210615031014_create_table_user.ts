import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("users", function (table) {
    table.increments("id");
    table.string("user_name").notNullable();
    table.string("password", 100).notNullable();
    table.string("name");
    table.string("lastname");
    table.string("description", 100);
    table.timestamps();
    table.timestamp("last_logon_date");
    table.timestamp("end_date");
    table.string("last_modified_user_id", 16);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("users");
}
