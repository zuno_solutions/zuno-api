import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("role", function (table) {
    table.increments("id");
    table.string("role", 40).notNullable();
    table.integer("expression_id").unsigned();
    table.integer("application");
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("role");
}
