import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("module_permission_rel", function (table) {
    table.increments("id");
    table.integer("permission_id").unsigned().notNullable();
    table.integer("module_id").unsigned().notNullable();
    table.string("granted", 1);
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("module_permission_rel");
}
