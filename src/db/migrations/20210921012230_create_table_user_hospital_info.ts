import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("user_hospital_info", function (table) {
    table.increments("id");
    table.integer("user_id").unsigned().notNullable();
    table.string("facilitylegalname");
    table.string("facilityname");
    table.string("physicaladdress");
    table.string("mainphone");
    table.string("operatingroomfloor");
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("user_hospital_info");
}
