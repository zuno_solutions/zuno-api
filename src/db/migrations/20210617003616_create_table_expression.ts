import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("expression", function (table) {
    table.increments("id");
    table.string("criteria", 40).notNullable();
    table.string("operator", 5).notNullable();
    table.string("value", 40).notNullable();
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("expression");
}
