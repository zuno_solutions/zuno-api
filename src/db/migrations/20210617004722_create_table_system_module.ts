import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("system_module", function (table) {
    table.increments("id");
    table.string("module_name", 40).notNullable();
    table.string("description", 100);
    table.string("status", 10).notNullable();
    table.integer("version_id").unsigned();
    table.string("last_modified_user_id", 16);
    table.string("path", 200);
    table.string("image", 200);
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("system_module");
}
