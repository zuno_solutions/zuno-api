import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("module_permission", function (table) {
    table.increments("id");
    table.string("permission", 40);
    table.string("description", 100);
    table.string("created_user_id", 16);
    table.timestamps();
    table.string("last_modified_user_id", 16);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("module_permission");
}
