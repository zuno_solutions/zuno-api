import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("group", function (table) {
    table.increments("id");
    table.string("group_name", 40).notNullable();
    table.string("membership", 100).notNullable();
    table.timestamps();
    table.string("last_modified_user_id", 16);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("group");
}
