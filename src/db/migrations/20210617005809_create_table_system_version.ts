import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("system_version", function (table) {
    table.increments("id");
    table.string("version", 40).notNullable();
    table.string("description", 100);
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("system_version");
}
