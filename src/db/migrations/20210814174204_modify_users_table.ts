import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // creating FK fk_role_expression
  await knex.schema.alterTable("users", function (table) {
    table.string("phone").unique;
    table.renameColumn("user_name", "username");
    table.boolean("isVerified").notNullable().defaultTo(false);
    table.string("verifyShortToken");
    table.string("verifyExpires");
    table.json("verifyChanges");
    table.string("resetShortToken");
    table.date("resetExpires");
    table.string("verifyToken");
    table.integer("resetAttempts");
    table.string("email");
    table.string("resetToken");
    table.string("mainphone");
    table.string("jobtitle");
    table.string("onboardingstatus");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("users", function (table) {
    table.dropColumn("phone");
    table.renameColumn("username", "user_name");
    table.dropColumn("isVerified");
    table.dropColumn("verifyShortToken");
    table.dropColumn("verifyExpires");
    table.dropColumn("verifyChanges");
    table.dropColumn("resetShortToken");
    table.dropColumn("resetExpires");
    table.dropColumn("verifyToken");
    table.dropColumn("resetAttempts");
    table.dropColumn("email");
    table.dropColumn("resetToken");
    table.string("mainphone");
    table.string("jobtitle");
    table.string("onboardingstatus");
  });
}
