import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("usergroup_role_rel", function (table) {
    table.increments("id");
    table.integer("user_id").unsigned();
    table.integer("group_id").unsigned();
    table.integer("role_id").unsigned().notNullable();
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("usergroup_role_rel");
}
