import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("systemmodule_role_rel", function (table) {
    table.increments("id");
    table.integer("role_id").unsigned();
    table.integer("module_id").unsigned();
    table.timestamps();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("systemmodule_role_rel");
}
