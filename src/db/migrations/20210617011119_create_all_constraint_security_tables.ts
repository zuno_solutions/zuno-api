import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // creating FK fk_role_expression
  await knex.schema.alterTable("role", function (table) {
    table
      .foreign("expression_id")
      .references("expression.id")
      .withKeyName("fk_role_expression");
  });

  // creating FK fk_system_module_system_version
  await knex.schema.alterTable("system_module", function (table) {
    table
      .foreign("version_id")
      .references("system_version.id")
      .withKeyName("fk_system_module_system_version");
  });

  // creating FK fk_systemmodule_role_rel_security_role
  await knex.schema.alterTable("systemmodule_role_rel", function (table) {
    table
      .foreign("role_id")
      .references("role.id")
      .withKeyName("fk_systemmodule_role_rel_security_role");
  });

  // creating FK fk_systemmodule_role_rel_system_module
  await knex.schema.alterTable("systemmodule_role_rel", function (table) {
    table
      .foreign("module_id")
      .references("system_module.id")
      .withKeyName("fk_systemmodule_role_rel_system_module");
  });

  // creating FK fk_user_group_rel_security_group
  await knex.schema.alterTable("user_group_rel", function (table) {
    table
      .foreign("group_id")
      .references("group.id")
      .withKeyName("fk_user_group_rel_security_group");
  });

  // creating FK fk_user_group_rel_security_user
  await knex.schema.alterTable("user_group_rel", function (table) {
    table
      .foreign("user_id")
      .references("users.id")
      .withKeyName("fk_user_group_rel_security_user");
  });

  // creating FK fk_usergroup_role_rel_security_group
  await knex.schema.alterTable("user_group_rel", function (table) {
    table
      .foreign("group_id")
      .references("group.id")
      .withKeyName("fk_usergroup_role_rel_security_group");
  });

  // creating FK fk_usergroup_role_rel_security_role
  await knex.schema.alterTable("usergroup_role_rel", function (table) {
    table
      .foreign("role_id")
      .references("role.id")
      .withKeyName("fk_usergroup_role_rel_security_role");
  });

  // creating FK fk_usergroup_role_rel_security_user
  await knex.schema.alterTable("usergroup_role_rel", function (table) {
    table
      .foreign("user_id")
      .references("users.id")
      .withKeyName("fk_usergroup_role_rel_security_user");
  });

  // creating FK module_permission_rel_module_permission_fk
  await knex.schema.alterTable("module_permission_rel", function (table) {
    table
      .foreign("permission_id")
      .references("module_permission.id")
      .withKeyName("module_permission_rel_module_permission_fk");
  });

  // creating FK module_permission_rel_system_module_fk
  return knex.schema.alterTable("module_permission_rel", function (table) {
    table
      .foreign("module_id")
      .references("system_module.id")
      .withKeyName("module_permission_rel_system_module_fk");
  });
  
}

export async function down(knex: Knex): Promise<void> {
  // dropping FK fk_role_expression
  await knex.schema.alterTable("role", function (table) {
    table.dropForeign("expression_id", "fk_role_expression");
  });

  // dropping FK fk_system_module_system_version
  await knex.schema.alterTable("system_module", function (table) {
    table.dropForeign("version_id", "fk_system_module_system_version");
  });

  // dropping FK fk_systemmodule_role_rel_security_role
  await knex.schema.alterTable("systemmodule_role_rel", function (table) {
    table.dropForeign("role_id", "fk_systemmodule_role_rel_security_role");
  });

  // dropping FK fk_systemmodule_role_rel_system_module
  await knex.schema.alterTable("systemmodule_role_rel", function (table) {
    table.dropForeign("module_id", "fk_systemmodule_role_rel_system_module");
  });

  // dropping FK fk_user_group_rel_security_group
  await knex.schema.alterTable("user_group_rel", function (table) {
    table.dropForeign("group_id", "fk_user_group_rel_security_group");
  });

  // dropping FK fk_user_group_rel_security_user
  await knex.schema.alterTable("user_group_rel", function (table) {
    table.dropForeign("user_id", "fk_user_group_rel_security_user");
  });

  // dropping FK fk_usergroup_role_rel_security_group
  await knex.schema.alterTable("user_group_rel", function (table) {
    table.dropForeign("group_id", "fk_usergroup_role_rel_security_group");
  });

  // dropping FK fk_usergroup_role_rel_security_role
  await knex.schema.alterTable("usergroup_role_rel", function (table) {
    table.dropForeign("role_id", "fk_usergroup_role_rel_security_role");
  });

  // dropping FK fk_usergroup_role_rel_security_user
  await knex.schema.alterTable("usergroup_role_rel", function (table) {
    table.dropForeign("user_id", "fk_usergroup_role_rel_security_user");
  });

  // dropping FK module_permission_rel_module_permission_fk
  await knex.schema.alterTable("module_permission_rel", function (table) {
    table.dropForeign("permission_id","module_permission_rel_module_permission_fk");
  });

  // dropping FK module_permission_rel_system_module_fk
  return knex.schema.alterTable("module_permission_rel", function (table) {
    table.dropForeign("module_id", "module_permission_rel_system_module_fk");
  });

}
